﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMove : MonoBehaviour {

    int BallJumpPower = 4000;
    int BallMovePower = 50;
    bool IsGrounded;
    Rigidbody Ball;

    void Start () {
        Ball = this.gameObject.GetComponent<Rigidbody>();
        Ball.mass = 10;
        Ball.drag = 1;
    }
	

	void Update () {
        if (Input.GetKeyDown(KeyCode.Space) && IsGrounded)
            Ball.AddForce(Vector3.up * BallJumpPower);
        if (Input.GetKey(KeyCode.W))
            Ball.AddForce(Vector3.forward * BallMovePower);
        if (Input.GetKey(KeyCode.S))
            Ball.AddForce(Vector3.back * BallMovePower);
        if (Input.GetKey(KeyCode.D))
            Ball.AddForce(Vector3.right * BallMovePower);
        if (Input.GetKey(KeyCode.A))
            Ball.AddForce(Vector3.left * BallMovePower);

    }
    private void OnCollisionEnter(Collision collision)
    {
        IsGrounded = true;
    }
    private void OnCollisionExit(Collision collision)
    {
        IsGrounded = false;
    }
}
